json.extract! statement, :id, :date, :reason, :nb_km, :taxe_coef, :ride, :start, :arrival, :user_id, :created_at, :updated_at
json.url statement_url(statement, format: :json)